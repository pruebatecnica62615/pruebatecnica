package com.prueba.tecnica.service.imp;

import com.prueba.tecnica.dto.EquipoGetDTO;
import com.prueba.tecnica.dto.EquiposDTO;
import com.prueba.tecnica.dto.Response;
import com.prueba.tecnica.entities.Equipos;
import com.prueba.tecnica.repository.EquiposRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.tecnica.service.EquiposService;

@Service
@Log
public class EquiposServiceImp implements EquiposService {


	@Autowired
	EquiposRepository equiposRepository;

	@Override
	public Response crearEquipo (EquiposDTO equiposDTO) {
		try {
		Equipos equipos = new Equipos();
		Response response = new Response();
		equipos.setNumeroSerial(equiposDTO.getNumero_serial());
		equipos.setDescripcion(equiposDTO.getDescripcion());
		equipos.setNombre(equiposDTO.getNombre());
		equipos.setFecha_compra(equiposDTO.getFecha_compra());
		equipos.setValor_compra(equiposDTO.getValor_compra());
		equiposRepository.save(equipos);
		log.info("Registro Guardado con Exito: " + equipos.getNombre());
		response.setCode(200);
		response.setDescription("Registro Guardado con Exito");
		response.setData(equipos);
		return response;
		}catch (Exception e){
			Response response = new Response();
			response.setCode(204);
			response.setDescription("Sin Resultados");
			return response;
		}
	}

	@Override
	public Response findBySerial (Integer numeroSerial){
		try {
			Response response = new Response();
			Equipos equipos = equiposRepository.findByNumeroSerial(numeroSerial);
			EquipoGetDTO equiposDTO = EquipoGetDTO.builder()
					.numero_serial(equipos.getNumeroSerial())
					.descripcion(equipos.getDescripcion())
					.nombre(equipos.getNombre())
					.fecha_compra(equipos.getFecha_compra())
					.valor_compra(equipos.getValor_compra())
					.valor_perdida((int) (equipos.getValor_compra()*0.04))
					.build();
			if (equiposDTO.getNumero_serial()!= null || !equiposDTO.equals(null)) {
				response.setCode(200);
				response.setDescription("Proceso Exitoso");
				response.setData(equiposDTO);
			}
			log.info("Registro Listado con Exito: " + equipos.getNombre());
			return response;

		}catch (Exception e){
			Response response = new Response();
			response.setCode(204);
			response.setDescription("Sin Resultados");
			return response;
		}
	}
	@Override
	public Response eliminarEquipo (Integer numeroSerial) {

		try {
			Response response = new Response();
			Equipos equipos = equiposRepository.findByNumeroSerial(numeroSerial);
			equiposRepository.delete(equipos);
			log.info("Registro Eliminado con Exito: " + equipos.getNombre());
			response.setCode(200);
			response.setDescription("Borrado Exitoso");
			response.setData(equipos);
			return response;

		} catch (Exception e) {
			Response response = new Response();
			response.setCode(204);
			response.setDescription("Sin Resultados para eliminar");
			return response;
		}
	}
	@Override
	public Response actualizarEquipo (EquiposDTO equiposDTO, Integer numeroSerial) {
		try {
			Response response = new Response();
			Equipos equipos = equiposRepository.findByNumeroSerial(numeroSerial);
			equipos.setNumeroSerial(equiposDTO.getNumero_serial());
			equipos.setDescripcion(equiposDTO.getDescripcion());
			equipos.setNombre(equiposDTO.getNombre());
			equipos.setFecha_compra(equiposDTO.getFecha_compra());
			equipos.setValor_compra(equiposDTO.getValor_compra());
			equiposRepository.save(equipos);
			log.info("Registro Actualizado con Exito: " + equipos.getNombre());
			response.setCode(200);
			response.setDescription("Actualizacion Exitosa");
			response.setData(equipos);
			return response;
		}catch (Exception e) {
			Response response = new Response();
			response.setCode(204);
			response.setDescription("No se pudo actualizar");
			return response;
		}
	}
}
