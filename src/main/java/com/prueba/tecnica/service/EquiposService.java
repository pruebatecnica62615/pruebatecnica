package com.prueba.tecnica.service;

import com.prueba.tecnica.dto.EquiposDTO;
import org.springframework.stereotype.Service;

import com.prueba.tecnica.dto.Response;

@Service
public interface EquiposService {


	Response findBySerial (Integer numero_serial_descripcion);

	Response eliminarEquipo (Integer numero_serial_descripcion);

	Response actualizarEquipo (EquiposDTO equiposDTO, Integer numero_serial_descripcion);

	Response crearEquipo (EquiposDTO equiposDTO);

}
