package com.prueba.tecnica.controller;

import com.prueba.tecnica.dto.EquiposDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.prueba.tecnica.dto.Response;
import com.prueba.tecnica.service.EquiposService;

@EnableAutoConfiguration
@CrossOrigin(origins = "*")
@RequestMapping(path = "${controller.properties.base-path}")
@RestController
@Controller
public class EquiposController {

	@Autowired
	private EquiposService equiposService;
	
	@PostMapping(value = "/crearEquipo")
	public Response crearEquipo (@RequestBody(required = true) EquiposDTO filmsDTO) {
	return equiposService.crearEquipo(filmsDTO);
	}

	@GetMapping(value = "/buscarEquipo/{numeroSerial}")
	public Response findFilm (@PathVariable(name="numeroSerial", required = false, value = "") Integer numeroSerial) {
		return equiposService.findBySerial(numeroSerial);
	}

	@PostMapping(value = "/borrarEquipo/{numeroSerial}")
	public Response eliminarEquipo (@PathVariable(name="numeroSerial", required = false, value = "") Integer numeroSerial) {
		return equiposService.eliminarEquipo(numeroSerial);
	}

	@PostMapping(value = "/actualizarEquipo/{numeroSerial}")
	public Response actualizarEquipo (@PathVariable(name="numeroSerial", required = false, value = "") Integer numeroSerial,
								@RequestBody(required = true) EquiposDTO equiposDTO) {
		return equiposService.actualizarEquipo(equiposDTO,numeroSerial);
	}
}
