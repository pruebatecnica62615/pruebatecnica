package com.prueba.tecnica.repository;

import com.prueba.tecnica.entities.Equipos;
import org.springframework.data.repository.CrudRepository;
import com.prueba.tecnica.entities.EquiposPK;

public interface EquiposRepository extends CrudRepository<Equipos, EquiposPK>{

    Equipos findByNumeroSerial(Integer numeroSerial);
}
