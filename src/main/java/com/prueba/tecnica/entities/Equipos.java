package com.prueba.tecnica.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "Equipos")
@IdClass(EquiposPK.class)
@Setter
@Getter
@Data
public class Equipos implements Serializable{
	
	private static final long serialVersionUID = -9005522915247534874L;

	@Id
	@Column(nullable = false, name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(nullable = false, name = "numero_serial")
	private Integer numeroSerial;

	@Column(nullable = false, name = "descripcion")
	private String descripcion;

	@Column(nullable = false, name = "nombre")
	private String nombre;

	@Column(nullable = false, name = "fecha_compra")
	private String fecha_compra;

	@Column(nullable = false, name = "valor_compra")
	private Integer valor_compra;

}
