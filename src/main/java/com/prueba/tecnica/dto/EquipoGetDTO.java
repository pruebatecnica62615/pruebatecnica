package com.prueba.tecnica.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class EquipoGetDTO {

    private Integer numero_serial;

    private String descripcion;

    private String nombre;

    private String fecha_compra;

    private Integer valor_compra;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Integer valor_perdida;

    public EquipoGetDTO(){

    }
}

