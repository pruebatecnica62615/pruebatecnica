package com.prueba.tecnica.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Component
public class Response {

	@JsonProperty("code")
	private Integer code;
	@JsonProperty("description")
	private String description;

	@JsonProperty("data")
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private Object data;

	@Override
	public String toString() {
		return "{\"code\":\"" + code + "\", \"description\":\"" + description + "\", \"data\":\"" + data + "\"}";
	}

}