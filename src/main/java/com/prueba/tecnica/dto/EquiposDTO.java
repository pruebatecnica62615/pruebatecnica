package com.prueba.tecnica.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class EquiposDTO {

	private Integer numero_serial;

	private String descripcion;

	private String nombre;

	private String fecha_compra;

	private Integer valor_compra;

	public EquiposDTO(){

	}
}
