package com.prueba.tecnica.controller;

import com.prueba.tecnica.dto.EquiposDTO;
import com.prueba.tecnica.dto.Response;
import com.prueba.tecnica.service.EquiposService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class EquiposControllerTest {

    @InjectMocks
    private EquiposController equiposController;

    @Mock
    private EquiposService equiposService;

    @BeforeEach

    public void setUp () {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void crearEquipoTest()  {
        when(equiposService.crearEquipo(any())).thenReturn(new Response(200,"prueba","prueba2"));
        Response response = equiposController.crearEquipo(new EquiposDTO());
        assertEquals(200, response.getCode());
        assertEquals("prueba", response.getDescription());
    }

    @Test
    void findSerialTest ()  {
        when(equiposService.findBySerial(any())).thenReturn(new Response(200,"prueba","prueba2"));
        Response response = equiposController.findFilm(1);
        assertEquals(200, response.getCode());
        assertEquals("prueba", response.getDescription());
    }

    @Test
    void EliminarEquipoTest()  {
        when(equiposService.eliminarEquipo(any())).thenReturn(new Response(200,"prueba","prueba2"));
        Response response = equiposController.eliminarEquipo(1);
        assertEquals(200, response.getCode());
        assertEquals("prueba", response.getDescription());
    }

    @Test
    void actualizarEquipoTest()  {
        when(equiposService.actualizarEquipo(any(),any())).thenReturn(new Response(200,"prueba","prueba2"));
        Response response = equiposController.actualizarEquipo(1,null);
        assertEquals(200, response.getCode());
        assertEquals("prueba", response.getDescription());
    }
}
