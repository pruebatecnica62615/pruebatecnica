package com.prueba.tecnica.dto;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

public class EquipoGetDTOTest {

    @InjectMocks
    private EquipoGetDTO equiposDTO = new EquipoGetDTO();

    @Test
    void validateNumeroSerial()
    {
        equiposDTO.setNumero_serial(1);
        Assert.assertEquals("1", equiposDTO.getNumero_serial().toString());
    }

    @Test
    void validateDescripcion()
    {
        equiposDTO.setDescripcion("prueba0");
        Assert.assertEquals("prueba0", equiposDTO.getDescripcion());
    }

    @Test
    void validateNombre()
    {
        equiposDTO.setNombre("prueba");
        Assert.assertEquals("prueba", equiposDTO.getNombre());
    }

    @Test
    void validateFecha_Compra()
    {
        equiposDTO.setFecha_compra("prueba2");
        Assert.assertEquals("prueba2", equiposDTO.getFecha_compra());
    }

    @Test
    void validateValor_Compra()
    {
        equiposDTO.setValor_compra(25000);
        Assert.assertEquals("25000", equiposDTO.getValor_compra().toString());
    }

    @Test
    void validateValor_Perdida()
    {
        equiposDTO.setValor_perdida(25000);
        Assert.assertEquals("25000", equiposDTO.getValor_perdida().toString());
    }

    @Test
    void validateBuilder()
    {
        EquipoGetDTO build = EquipoGetDTO.builder()
                .numero_serial(1)
                .descripcion("prueba0")
                .nombre("prueba")
                .fecha_compra("prueba2")
                .valor_compra(25000)
                .valor_perdida(25000)
                .build();
        Assert.assertNotNull(build.toString());
    }
}
