package com.prueba.tecnica.service;

import com.prueba.tecnica.dto.EquiposDTO;
import com.prueba.tecnica.dto.Response;
import com.prueba.tecnica.entities.Equipos;
import com.prueba.tecnica.repository.EquiposRepository;
import com.prueba.tecnica.service.imp.EquiposServiceImp;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
class EquiposServiceTest {

    @Mock
    private EquiposRepository equiposRepository;

    @InjectMocks
    private EquiposServiceImp equiposServiceImp;

    private Response responseInicial = new Response();

    private Equipos equipos = new Equipos();

    public void setUp () {
        MockitoAnnotations.initMocks(this);

        responseInicial.setCode(200);
        responseInicial.setDescription("Exitoso");
        responseInicial.setData("prueba");

        equipos.setNumeroSerial(1);
        equipos.setDescripcion("prueba");
        equipos.setNombre("prueba");
        equipos.setFecha_compra("23/06/2024");
        equipos.setValor_compra(25000);
    }

    @Test
    public void createFilmsTest() {

        Response response = new Response();
        response.setCode(200);
        response.setDescription("Exitoso");
        response.setData("prueba");
        when(equiposRepository.save(any())).thenAnswer(invocation -> invocation.getArgument(0));
        Response result = equiposServiceImp.crearEquipo(new EquiposDTO());
        verify(equiposRepository, times(1)).save(any(Equipos.class));
        assertEquals(response.getCode(), result.getCode());
    }


    @Test
    void findByIdTest () {
        when(equiposRepository.findByNumeroSerial(any())).thenReturn(equipos);
        Response response = equiposServiceImp.findBySerial(1);
        assertEquals(response.getDescription(), response.getDescription());
    }

    @Test
    void findByIdCatchTest () {
        when(equiposRepository.findByNumeroSerial(any())).thenThrow(RuntimeException.class);
        Response response = equiposServiceImp.findBySerial(1);
        assertEquals("Sin Resultados", response.getDescription());
    }


    @Test
    void deleteFilmsTest () {
        when(equiposRepository.findByNumeroSerial(any())).thenReturn(equipos);
        Response response = equiposServiceImp.eliminarEquipo(1);
        assertEquals(response.getDescription(), response.getDescription());
    }

    @Test
    void deleteFilmsCatchTest() {
        when(equiposRepository.findByNumeroSerial(any())).thenThrow(RuntimeException.class);
        Response response = equiposServiceImp.eliminarEquipo(1);
        assertEquals("Sin Resultados para eliminar", response.getDescription());
    }


    @Test
    void updateFilmTest () {
        when(equiposRepository.findByNumeroSerial(any())).thenReturn(equipos);
        Response response = equiposServiceImp.actualizarEquipo(new EquiposDTO(),1);
        assertEquals(response.getDescription(), response.getDescription());
    }

    @Test
    void updateFilmCatchTest () {
        when(equiposRepository.findByNumeroSerial(any())).thenThrow(RuntimeException.class);
        Response response = equiposServiceImp.actualizarEquipo(new EquiposDTO(),1);
        assertEquals("No se pudo actualizar", response.getDescription());
    }
}
